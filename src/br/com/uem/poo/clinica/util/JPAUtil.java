/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.uem.poo.clinica.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernate");

    public static EntityManagerFactory getEntityManagerFactory(){
        return emf;
    }

    public static EntityManager getEntityManager(){
        return emf.createEntityManager();
    }

    public static void fechaEntityManagerFactory(EntityManagerFactory emf){
        if(emf!=null)
            emf.close();
    }

    public static void fechaEntityManager(EntityManager em){
        if(em!=null)
            em.close();
    }
}
