package br.com.uem.poo.clinica.entidade;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Contato  implements Serializable {
  @Id @GeneratedValue(strategy = GenerationType.AUTO)
  Long id;
  private String endereco;
  private String bairro;
  private String cidade;
  private String email;
  private String telefone;

  public Contato() {
    super();
  }

  public Contato(Long id, String endereco, String bairro, String cidade, String email, String telefone) {
    this.id = id;
    this.endereco = endereco;
    this.bairro = bairro;
    this.cidade = cidade;
    this.email = email;
    this.telefone = telefone;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEndereco() {
    return endereco;
  }

  public void setEndereco(String endereco) {
    this.endereco = endereco;
  }

  public String getBairro() {
    return bairro;
  }

  public void setBairro(String bairro) {
    this.bairro = bairro;
  }

  public String getCidade() {
    return cidade;
  }

  public void setCidade(String cidade) {
    this.cidade = cidade;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTelefone() {
    return telefone;
  }

  public void setTelefone(String telefone) {
    this.telefone = telefone;
  }

  public static class ContatoBuilder{
    private Long id;
    private String endereco;
    private String bairro;
    private String cidade;
    private String email;
    private String telefone;

    public ContatoBuilder(){
      super();
    }

    public ContatoBuilder id(Long id){
      this.id = id;
      return this;
    }

    public ContatoBuilder endereco(String endereco){
      this.endereco = endereco;
      return this;
    }

    public ContatoBuilder bairro(String bairro){
      this.bairro = bairro;
      return this;
    }

    public ContatoBuilder cidade(String cidade){
      this.cidade = cidade;
      return this;
    }

    public ContatoBuilder email(String email){
      this.email = email;
      return this;
    }

    public ContatoBuilder telefone(String telefone){
      this.telefone = telefone;
      return this;
    }

    public Contato build(){
      return new Contato(id, endereco,  bairro, cidade, email, telefone );
    }

  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Contato contato = (Contato) o;
    return Objects.equals(endereco, contato.endereco) && Objects.equals(bairro, contato.bairro) && Objects.equals(cidade, contato.cidade) && Objects.equals(email, contato.email) && Objects.equals(telefone, contato.telefone);
  }

  @Override
  public int hashCode() {
    return Objects.hash(endereco, bairro, cidade, email, telefone);
  }
}
