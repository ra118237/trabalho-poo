package br.com.uem.poo.clinica.entidade;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity
public class Paciente  implements Serializable {
  private static  Long numeroPaciente = 0L;

  @Id @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String nome;
  @OneToOne(cascade = CascadeType.ALL)
  private Contato contato;
  private LocalDate dataNascimento;
  private String sexo;
  private String estadoCivil;
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "paciente")
  private List<DadosAdicionaisPaciente> dadosAdicionais;
  private String tipoConvenio;

  public Paciente() {
    super();
  }

  public Paciente(Long id, String nome, Contato contato, LocalDate dataNascimento, String sexo, String estadoCivil, String tipoConvenio) {
    this.id = id;
    this.nome = nome;
    this.contato = contato;
    this.dataNascimento = dataNascimento;
    this.sexo = sexo;
    this.estadoCivil = estadoCivil;
    this.tipoConvenio = tipoConvenio;
  }

  public static Long getNumeroPaciente() {
    return numeroPaciente;
  }

  public static void setNumeroPaciente(Long numeroPaciente) {
    Paciente.numeroPaciente = numeroPaciente;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public Contato getContato() {
    return contato;
  }

  public void setContato(Contato contato) {
    this.contato = contato;
  }

  public LocalDate getDataNascimento() {
    return dataNascimento;
  }

  public void setDataNascimento(LocalDate dataNascimento) {
    this.dataNascimento = dataNascimento;
  }

  public String getSexo() {
    return sexo;
  }

  public void setSexo(String sexo) {
    this.sexo = sexo;
  }

  public String getEstadoCivil() {
    return estadoCivil;
  }

  public void setEstadoCivil(String estadoCivil) {
    this.estadoCivil = estadoCivil;
  }

  public String getTipoConvenio() {
    return tipoConvenio;
  }

  public void setTipoConvenio(String tipoConvenio) {
    this.tipoConvenio = tipoConvenio;
  }

public List<DadosAdicionaisPaciente> getDadosAdicionais() {
    return dadosAdicionais;
}

public void setDadosAdicionais(List<DadosAdicionaisPaciente> dadosAdicionais) {
    this.dadosAdicionais = dadosAdicionais;
}

  public static class PacienteBuilder{
    private Long id;
    private String nome;
    private Contato contato;
    private LocalDate dataNascimento;
    private String sexo;
    private String estadoCivil;
    private String tipoConvenio;

    public PacienteBuilder() {
      super();
    }

    public PacienteBuilder nome(String nome){
      this.nome = nome;
      return this;
    }

    public PacienteBuilder endereco(String nome){
      this.nome = nome;
      return this;
    }

    public PacienteBuilder contato(Contato contato){
      this.contato = contato;
      return this;
    }

    public PacienteBuilder dataNascimento(LocalDate dataNascimento){
      this.dataNascimento = dataNascimento;
      return this;
    }

    public PacienteBuilder sexo(String sexo){
      this.sexo = sexo;
      return this;
    }

    public PacienteBuilder estadoCivil(String estadoCivil){
      this.estadoCivil = estadoCivil;
      return this;
    }

    public PacienteBuilder tipoConvenio(String tipoConvenio){
      this.tipoConvenio = tipoConvenio;
      return this;
    }

    public PacienteBuilder id(Long id){
      this.id = id;
      return this;
    }

    public Paciente build(){
      return new Paciente(id, nome, contato, dataNascimento, sexo, estadoCivil, tipoConvenio);
    }

  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Paciente paciente = (Paciente) o;
    return id.equals(paciente.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
