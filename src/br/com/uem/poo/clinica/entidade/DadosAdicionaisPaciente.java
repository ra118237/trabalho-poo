package br.com.uem.poo.clinica.entidade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "dadosadicionais")
public class DadosAdicionaisPaciente implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Long id;
  @ManyToOne(fetch = FetchType.EAGER)
  Paciente paciente;
  private String dadoAdicionai;

  public DadosAdicionaisPaciente() {
  }

    public DadosAdicionaisPaciente(Long id, Paciente paciente, String dadoAdicionai) {
        this.id = id;
        this.paciente = paciente;
        this.dadoAdicionai = dadoAdicionai;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public String getDadoAdicional() {
        return dadoAdicionai;
    }

    public void setDadoAdicionai(String dadoAdicionai) {
        this.dadoAdicionai = dadoAdicionai;
    }
  
  

}
