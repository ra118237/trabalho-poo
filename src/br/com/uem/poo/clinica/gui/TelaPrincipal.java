/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.uem.poo.clinica.gui;

import br.com.uem.poo.clinica.gui.janelas.JanelaMedico;
import br.com.uem.poo.clinica.gui.janelas.JanelaMensagem;
import br.com.uem.poo.clinica.gui.janelas.JanelaSecretaria;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import org.jboss.jandex.JandexAntTask;

/**
 *
 * @author usuario
 */
public class TelaPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form TelaPrincipal
     */
    public TelaPrincipal() {
        setIconImage(new ImageIcon(getClass().getResource("/midia/logo.png")).getImage());
       
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        jTBarraFerramentas = new javax.swing.JToolBar();
        SecretariaButton = new javax.swing.JButton();
        MedicoCategoria = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        desktop = new javax.swing.JDesktopPane();
        planoDeFundo = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("br/com/uem/poo/clinica/gui/Bundle"); // NOI18N
        jMenuItem1.setText(bundle.getString("TelaPrincipal.jMenuItem1.text")); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle(bundle.getString("TelaPrincipal.title")); // NOI18N
        setMaximumSize(new java.awt.Dimension(592, 544));
        setMinimumSize(new java.awt.Dimension(592, 544));
        setPreferredSize(new java.awt.Dimension(900, 1000));

        jTBarraFerramentas.setFloatable(false);
        jTBarraFerramentas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jTBarraFerramentas.setMaximumSize(new java.awt.Dimension(18, 60));
        jTBarraFerramentas.setMinimumSize(new java.awt.Dimension(18, 60));
        jTBarraFerramentas.setOpaque(false);
        jTBarraFerramentas.setPreferredSize(new java.awt.Dimension(100, 60));

        SecretariaButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/secretaria.png"))); // NOI18N
        SecretariaButton.setText(bundle.getString("TelaPrincipal.SecretariaButton.text")); // NOI18N
        SecretariaButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        SecretariaButton.setFocusable(false);
        SecretariaButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        SecretariaButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        SecretariaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SecretariaButtonActionPerformed(evt);
            }
        });
        jTBarraFerramentas.add(SecretariaButton);

        MedicoCategoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/doctor.png"))); // NOI18N
        MedicoCategoria.setText(bundle.getString("TelaPrincipal.MedicoCategoria.text")); // NOI18N
        MedicoCategoria.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        MedicoCategoria.setFocusable(false);
        MedicoCategoria.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        MedicoCategoria.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        MedicoCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MedicoCategoriaActionPerformed(evt);
            }
        });
        jTBarraFerramentas.add(MedicoCategoria);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/carta.png"))); // NOI18N
        jButton2.setText(bundle.getString("TelaPrincipal.jButton2.text")); // NOI18N
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setLabel(bundle.getString("TelaPrincipal.jButton2.label")); // NOI18N
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jTBarraFerramentas.add(jButton2);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/sair.png"))); // NOI18N
        jButton1.setText(bundle.getString("TelaPrincipal.jButton1.text")); // NOI18N
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jTBarraFerramentas.add(jButton1);

        getContentPane().add(jTBarraFerramentas, java.awt.BorderLayout.PAGE_START);

        desktop.setLayer(planoDeFundo, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout desktopLayout = new javax.swing.GroupLayout(desktop);
        desktop.setLayout(desktopLayout);
        desktopLayout.setHorizontalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(desktopLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(planoDeFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(48, 48, 48))
        );
        desktopLayout.setVerticalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, desktopLayout.createSequentialGroup()
                .addGap(0, 63, Short.MAX_VALUE)
                .addComponent(planoDeFundo, javax.swing.GroupLayout.PREFERRED_SIZE, 700, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(desktop, java.awt.BorderLayout.CENTER);
        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SecretariaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SecretariaButtonActionPerformed
       try{
            JanelaSecretaria janelaSecretaria=new JanelaSecretaria();
            janelaSecretaria.setSize(900, 600);
            janelaSecretaria.setFocusable(true);
            desktop.add(janelaSecretaria);
        janelaSecretaria.setVisible(true);
          }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_SecretariaButtonActionPerformed

    private void MedicoCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MedicoCategoriaActionPerformed
       try{
            JanelaMedico janelaMedico=new JanelaMedico();
            janelaMedico.setSize(840, 506);
            janelaMedico.setFocusable(true);
            desktop.add(janelaMedico);
            janelaMedico.setVisible(true);
         }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_MedicoCategoriaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       try{
            JanelaMensagem janelaMensagem=new JanelaMensagem();
            janelaMensagem.setSize(600, 400);
            janelaMensagem.setFocusable(true);
            desktop.add(janelaMensagem);
        janelaMensagem.setVisible(true);
          }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton MedicoCategoria;
    private javax.swing.JButton SecretariaButton;
    private javax.swing.JDesktopPane desktop;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JToolBar jTBarraFerramentas;
    private javax.swing.JLabel planoDeFundo;
    // End of variables declaration//GEN-END:variables
}
