/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.uem.poo.clinica.gui.janelas.painel.medico;

import br.com.uem.poo.clinica.gui.janelas.painel.secretaria.*;
import br.com.uem.poo.clinica.entidade.Consulta;
import br.com.uem.poo.clinica.entidade.Contato;
import br.com.uem.poo.clinica.entidade.DadosAdicionaisPaciente;
import br.com.uem.poo.clinica.entidade.Paciente;
import br.com.uem.poo.clinica.entidade.Prontuario;
import br.com.uem.poo.clinica.relatorio.Relatorio;
import br.com.uem.poo.clinica.relatorio.medico.AtestadoMedico;
import br.com.uem.poo.clinica.relatorio.medico.DeclaracaoDeAcompanhamento;
import br.com.uem.poo.clinica.relatorio.medico.PacientesAtendidos;
import br.com.uem.poo.clinica.relatorio.medico.ReceitaMedica;
import br.com.uem.poo.clinica.servicos.Medico;
import br.com.uem.poo.clinica.servicos.Secretaria;
import br.com.uem.poo.clinica.util.DateTimeUtil;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;
import org.hibernate.dialect.DB2390Dialect;

/**
 *
 * @author johnwill
 */
public class PainelRelatorioMedico extends javax.swing.JPanel {

      /**
     * Creates new form JanelaSecretaria
     */
    public PainelRelatorioMedico() {
        initComponents();
       
    }
    
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jBClienteMes = new javax.swing.JButton();
        jBAcompanhante = new javax.swing.JButton();
        jBAtestado = new javax.swing.JButton();
        jBReceita = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jBClienteMes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/recepcao.png"))); // NOI18N
        jBClienteMes.setText("clientes do mês");
        jBClienteMes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jBClienteMes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBClienteMesActionPerformed(evt);
            }
        });
        jPanel2.add(jBClienteMes, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 230, 270, -1));

        jBAcompanhante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/acompanhamento.png"))); // NOI18N
        jBAcompanhante.setText("Gera acompanhamento");
        jBAcompanhante.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jBAcompanhante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAcompanhanteActionPerformed(evt);
            }
        });
        jPanel2.add(jBAcompanhante, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 140, -1, -1));

        jBAtestado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/atestado.png"))); // NOI18N
        jBAtestado.setText("Gera atestado");
        jBAtestado.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jBAtestado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAtestadoActionPerformed(evt);
            }
        });
        jPanel2.add(jBAtestado, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 230, 270, -1));

        jBReceita.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/receita.png"))); // NOI18N
        jBReceita.setText("Gera receita");
        jBReceita.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jBReceita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBReceitaActionPerformed(evt);
            }
        });
        jPanel2.add(jBReceita, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 140, 270, -1));

        add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 30, 730, 440));
    }// </editor-fold>//GEN-END:initComponents

    private void jBAcompanhanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAcompanhanteActionPerformed
        String acompanhante = JOptionPane.showInputDialog(this, "Digite o nome do acompnhante");
        String paciente = JOptionPane.showInputDialog(this, "Digite o nome do paciente");
         
        Relatorio rel = new DeclaracaoDeAcompanhamento(paciente, acompanhante, LocalDate.now());
        
         JOptionPane.showMessageDialog(this, rel.geraRelatorio(), "Declaracao gerada !", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jBAcompanhanteActionPerformed

    private void jBReceitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBReceitaActionPerformed
        String paciente = JOptionPane.showInputDialog(this, "Digite o nome do paciente");
        String medicamento = JOptionPane.showInputDialog(this, "Digite o nome do medicamento");
        String detalheDaMedicacao = JOptionPane.showInputDialog(this, "Digite os detalhes do medicamento");
        String nomeMedico = JOptionPane.showInputDialog(this, "Digite o nome do medico");     
        
        Relatorio rel = new ReceitaMedica(paciente, medicamento, detalheDaMedicacao, nomeMedico, LocalDate.now());
        
        JOptionPane.showMessageDialog(this, rel.geraRelatorio(), "Receita Gerada !", JOptionPane.INFORMATION_MESSAGE);
 
    }//GEN-LAST:event_jBReceitaActionPerformed

    private void jBAtestadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAtestadoActionPerformed
        String paciente = JOptionPane.showInputDialog(this, "Digite o nome do paciente");
        Relatorio rel = new AtestadoMedico(paciente, LocalDate.now());
        
        JOptionPane.showMessageDialog(this, rel.geraRelatorio(), "Atestado Gerado !", JOptionPane.INFORMATION_MESSAGE);

    }//GEN-LAST:event_jBAtestadoActionPerformed

    private void jBClienteMesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBClienteMesActionPerformed
        Medico m = new Medico();
        String nomeMedico = JOptionPane.showInputDialog(this, "Digite o nome do medico"); 
        List<Paciente> pacientes = m.listaPacientesDeUmMedicoAtendidosNoMesAtual(nomeMedico);
        
        Relatorio rel = new PacientesAtendidos(nomeMedico, pacientes);
        JOptionPane.showMessageDialog(this, rel.geraRelatorio(), "Relatorio Gerado !", JOptionPane.INFORMATION_MESSAGE);
 
    }//GEN-LAST:event_jBClienteMesActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JButton jBAcompanhante;
    private javax.swing.JButton jBAtestado;
    private javax.swing.JButton jBClienteMes;
    private javax.swing.JButton jBReceita;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
