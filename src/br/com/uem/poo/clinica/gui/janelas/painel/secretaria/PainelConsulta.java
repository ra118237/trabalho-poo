/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.uem.poo.clinica.gui.janelas.painel.secretaria;

import br.com.uem.poo.clinica.entidade.Consulta;
import br.com.uem.poo.clinica.entidade.Contato;
import br.com.uem.poo.clinica.entidade.Paciente;
import br.com.uem.poo.clinica.servicos.Secretaria;
import br.com.uem.poo.clinica.util.DateTimeUtil;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author johnwill
 */
public class PainelConsulta extends javax.swing.JPanel {

      /**
     * Creates new form JanelaSecretaria
     */
    public PainelConsulta() {
        initComponents();
        secretaria = new Secretaria();
        myCustomizations();
    }
    
    
   private  void  myCustomizations(){
            jCPessoa.removeAllItems();
            secretaria.listaPacientes().forEach(p -> {
                jCPessoa.addItem(p.getId()+"-"+p.getNome());
        });
            
           buttonGroup.add(jRNormal);
           buttonGroup.add(jRRetorno);
           
           jRNormal.setSelected(true);
           
           try {
               MaskFormatter formatterDataTime = new MaskFormatter("##/##/#### - ##:##");
               
               formatterDataTime.install(jFDataHorario);
             } catch (ParseException ex) {
                 throw new RuntimeException(ex);
             }
            
            criaTabela();
            carregaTabela();
    }
    
   
    
    private  void criaTabela(){
        modelTabelaPessoas =new DefaultTableModel(new Object[][]{}, new String[]{
            "ID","Nome", "Tipo", "Horario", "Medico"
        });
        tabelaPacientes.setModel(modelTabelaPessoas);
    }
    
    private  void carregaTabela(){
        List<Consulta> consultas = secretaria.listaConsulta();
        completaTabela(consultas);
        
    }
    
    private  void carregaTabela(String str){
       List<Consulta> consultas = secretaria.listaConsultaPeloNomePaciente(str);
        completaTabela(consultas);
        
    }
    
    private  void completaTabela(List<Consulta> consultas){
       limpaTabela();
       for(Consulta c:consultas){
           String dataHora = DateTimeUtil.converteLocalDateTimeParaString(c.getDiaHorario(), "dd/MM/yyyy - HH:mm");
            modelTabelaPessoas.addRow(new Object[]{
               c.getId(),
                c.getPaciente().getNome(),
                c.getDuracao()==60?"Normal":"Retorno",
                dataHora,
                c.getNomeMedico()
            });
        }
    }
    private  void limpaTabela(){
        int length=modelTabelaPessoas.getRowCount();
        for(int i=0;i<length;i++){
            modelTabelaPessoas.removeRow(0);
            
        }
    }
    private  void selecionaRow(){
        if(tabelaPacientes.getSelectedRow()!=-1){
            int row=tabelaPacientes.getSelectedRow();
            String idString = tabelaPacientes.getValueAt(row, 0).toString();
            Long id = Long.parseLong(idString);
            Consulta c =secretaria.buscaConsultaPeloId(id);
            
            jTId.setText(c.getId().toString());
            if(c.getDuracao()==30){
               jRRetorno.setSelected(true);
            }else{
                jRNormal.setSelected(true);
            }
            
            for(int i =0; i<jCPessoa.getItemCount();i++){
                
                 String str = jCPessoa.getItemAt(i);
                 if(str.contains(c.getPaciente().getNome())){
                     jCPessoa.setSelectedIndex(i);
                     break;
                 }
            }
            
            
            jTMedico.setText(c.getNomeMedico());
            jFDataHorario.setText(DateTimeUtil.converteLocalDateTimeParaString(c.getDiaHorario(), "dd/MM/yyyy - HH:mm"));
        
       }
    }
    
    private Consulta capturaConsulta(){
       
        Consulta.ConsultaBuild cb = new Consulta.ConsultaBuild();
        LocalDateTime ldt = DateTimeUtil.converteStringParaLocalDateTime(jFDataHorario.getText(), "dd/MM/yyyy - HH:mm");
        String pessoaString = jCPessoa.getSelectedItem().toString();
        String idPAcienteString = pessoaString.substring(0,pessoaString.indexOf("-"));
        int duracao;
        if(jRRetorno.isSelected()){
            duracao=30;
        }else{
            duracao=60;
        }
        
        
        Long id = jTId.getText().isEmpty()?null:Long.parseLong(jTId.getText()); 
        
        Consulta consulta = cb.id(id)
                .paciente(secretaria.buscaPacientePeloId(Long.parseLong(idPAcienteString)))
                .diaHorario(ldt)
                .duracao(duracao)
                .nomeMedico(jTMedico.getText())
                .build();
        
        return consulta;    
    }
    
    void limpaCampos(){
         jTId.setText("");
         jFDataHorario.setText("");
         jFDataHorario.setText("");
         jRNormal.setSelected(true);
         jTMedico.setText("");
         jCPessoa.setSelectedIndex(0);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        painelDadosPaciente = new javax.swing.JPanel();
        dados = new javax.swing.JPanel();
        jLId = new javax.swing.JLabel();
        jTId = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jCPessoa = new javax.swing.JComboBox<>();
        jTMedico = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jRRetorno = new javax.swing.JRadioButton();
        jRNormal = new javax.swing.JRadioButton();
        jFDataHorario = new javax.swing.JFormattedTextField();
        painelDados = new javax.swing.JPanel();
        painelOperacoes = new javax.swing.JPanel();
        jBSalvar = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBLimpar = new javax.swing.JButton();
        painelOp = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaPacientes = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();
        jTextPesquisa = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        painelDadosPaciente.setBackground(new java.awt.Color(255, 255, 255));
        painelDadosPaciente.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados Paciente"));
        painelDadosPaciente.setLayout(new java.awt.BorderLayout());

        dados.setBackground(new java.awt.Color(255, 255, 255));

        jLId.setText("ID");

        jTId.setEnabled(false);

        jLabel2.setText("Paciente");

        jLabel1.setText("Nome medico");

        jCPessoa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCPessoa.setToolTipText("");
        jCPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCPessoaActionPerformed(evt);
            }
        });

        jLabel11.setText("Data horario");

        jLabel12.setText("Tipo de consulta");

        jRRetorno.setText("Retorno");
        jRRetorno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRRetornoActionPerformed(evt);
            }
        });

        jRNormal.setText("Normal");
        jRNormal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRNormalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dadosLayout = new javax.swing.GroupLayout(dados);
        dados.setLayout(dadosLayout);
        dadosLayout.setHorizontalGroup(
            dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dadosLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addComponent(jLId)
                        .addGap(40, 40, 40)
                        .addComponent(jTId, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 374, Short.MAX_VALUE)
                        .addComponent(jLabel1))
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel11))
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(18, 18, 18)
                        .addComponent(jRNormal)
                        .addGap(26, 26, 26)
                        .addComponent(jRRetorno)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(34, 34, 34)
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTMedico, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                    .addComponent(jFDataHorario))
                .addGap(64, 64, 64))
        );
        dadosLayout.setVerticalGroup(
            dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dadosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLId)
                    .addComponent(jTId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jTMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jCPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)))
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jFDataHorario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jRRetorno)
                    .addComponent(jRNormal)))
        );

        painelDadosPaciente.add(dados, java.awt.BorderLayout.PAGE_START);

        painelDados.setLayout(new java.awt.BorderLayout());

        painelOperacoes.setBackground(new java.awt.Color(255, 255, 255));
        painelOperacoes.setBorder(javax.swing.BorderFactory.createTitledBorder("Operacoes"));

        jBSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/novo-foco.png"))); // NOI18N
        jBSalvar.setText("Salvar");
        jBSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSalvarActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBSalvar);

        jBAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/salvar-foco.png"))); // NOI18N
        jBAlterar.setText("Alterar");
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBAlterar);

        jBExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/excluir-foco.png"))); // NOI18N
        jBExcluir.setText("Excluir");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBExcluir);

        jBLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/apagador.png"))); // NOI18N
        jBLimpar.setText("Limpar");
        jBLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLimparActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBLimpar);

        painelDados.add(painelOperacoes, java.awt.BorderLayout.CENTER);

        painelOp.setBackground(new java.awt.Color(255, 255, 255));
        painelOp.setBorder(javax.swing.BorderFactory.createTitledBorder("Tabela Consultas"));
        painelOp.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jScrollPane1KeyPressed(evt);
            }
        });

        tabelaPacientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelaPacientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaPacientesMouseClicked(evt);
            }
        });
        tabelaPacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tabelaPacientesKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaPacientes);

        painelOp.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 830, 130));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/buscar-foco.png"))); // NOI18N
        painelOp.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 20, 30, 20));

        jTextPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextPesquisaActionPerformed(evt);
            }
        });
        jTextPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextPesquisaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextPesquisaKeyReleased(evt);
            }
        });
        painelOp.add(jTextPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 20, 500, -1));

        jLabel3.setText("Consulta pelo nome paciente");
        painelOp.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, -1, -1));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(painelDadosPaciente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(painelDados, javax.swing.GroupLayout.DEFAULT_SIZE, 833, Short.MAX_VALUE)
                    .addComponent(painelOp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(painelDadosPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelDados, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelOp, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 15, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void jCPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCPessoaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCPessoaActionPerformed

    private void tabelaPacientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaPacientesMouseClicked
        selecionaRow();
    }//GEN-LAST:event_tabelaPacientesMouseClicked

    private void jScrollPane1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jScrollPane1KeyPressed
        selecionaRow();
    }//GEN-LAST:event_jScrollPane1KeyPressed

    private void jBSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSalvarActionPerformed
        try{
            Consulta c = capturaConsulta();
            secretaria.adicionaConsulta(c);
            limpaCampos();
            carregaTabela();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBSalvarActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
       try{
            if(jTId.getText().equals("")){
                JOptionPane.showMessageDialog(null, "selecione um registro", "Erro", JOptionPane.ERROR_MESSAGE);

            }else{
                Consulta c = capturaConsulta();
                secretaria.atualizaConsulta(c);
                carregaTabela();
            }
            limpaCampos();
         }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
       try{
            Long id = jTId.getText().isEmpty()?null:Long.parseLong(jTId.getText());
            if(id==null){
                JOptionPane.showMessageDialog(null, "selecione um registro", "Erro", JOptionPane.ERROR_MESSAGE);

            }else{
                secretaria.removeConsulta(id);
                carregaTabela();
            }
            limpaCampos();
       } catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLimparActionPerformed
        limpaCampos();
    }//GEN-LAST:event_jBLimparActionPerformed

    private void jRRetornoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRRetornoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRRetornoActionPerformed

    private void jRNormalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRNormalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRNormalActionPerformed

    private void jTextPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextPesquisaKeyReleased
        carregaTabela(jTextPesquisa.getText());
    }//GEN-LAST:event_jTextPesquisaKeyReleased

    private void jTextPesquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextPesquisaKeyPressed

    }//GEN-LAST:event_jTextPesquisaKeyPressed

    private void jTextPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextPesquisaActionPerformed

    }//GEN-LAST:event_jTextPesquisaActionPerformed

    private void tabelaPacientesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaPacientesKeyReleased
        selecionaRow();
    }//GEN-LAST:event_tabelaPacientesKeyReleased

private  DefaultTableModel modelTabelaPessoas;
    private Secretaria secretaria;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JPanel dados;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBLimpar;
    private javax.swing.JButton jBSalvar;
    private javax.swing.JComboBox<String> jCPessoa;
    private javax.swing.JFormattedTextField jFDataHorario;
    private javax.swing.JLabel jLId;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRNormal;
    private javax.swing.JRadioButton jRRetorno;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTId;
    private javax.swing.JTextField jTMedico;
    private javax.swing.JTextField jTextPesquisa;
    private javax.swing.JPanel painelDados;
    private javax.swing.JPanel painelDadosPaciente;
    private javax.swing.JPanel painelOp;
    private javax.swing.JPanel painelOperacoes;
    private javax.swing.JTable tabelaPacientes;
    // End of variables declaration//GEN-END:variables
}
