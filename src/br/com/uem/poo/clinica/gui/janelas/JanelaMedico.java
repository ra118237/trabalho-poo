/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.uem.poo.clinica.gui.janelas;

import br.com.uem.poo.clinica.gui.janelas.painel.medico.PainelRelatorioMedico;
import br.com.uem.poo.clinica.gui.janelas.painel.medico.PainelDadosAdicionais;
import br.com.uem.poo.clinica.gui.janelas.painel.medico.PainelProntuario;
import br.com.uem.poo.clinica.gui.janelas.painel.secretaria.PainelConsulta;
import br.com.uem.poo.clinica.gui.janelas.painel.secretaria.PainelPaciente;
import br.com.uem.poo.clinica.gui.janelas.painel.secretaria.PainelRelatorioConsulta;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;



/**
 *
 * @author johnwill
 */
public class JanelaMedico extends javax.swing.JInternalFrame {

    /**
     * Creates new form JanelaSecretaria
     */
    public JanelaMedico() {
        initComponents();  
        myCustomizations();
       
    }
    
    
    void  myCustomizations(){
        JScrollPane prontuarioScrollPane = new JScrollPane(new PainelProntuario());
        jTabbedPane.add("Dados adicionais Paciente", new PainelDadosAdicionais());
        jTabbedPane.add("Prontuario Medico", prontuarioScrollPane);
        jTabbedPane.add("Relatório Consulta", new PainelRelatorioMedico());
    }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        buttonGroup = new javax.swing.ButtonGroup();
        jTabbedPane = new javax.swing.JTabbedPane();

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setClosable(true);
        setForeground(java.awt.Color.gray);
        setIconifiable(true);
        setTitle("Menu Medico");
        setToolTipText("");
        setAutoscrolls(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/doctor.png"))); // NOI18N
        try {
            setSelected(true);
        } catch (java.beans.PropertyVetoException e1) {
            e1.printStackTrace();
        }
        setVisible(true);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));
        getContentPane().add(jTabbedPane);
        jTabbedPane.getAccessibleContext().setAccessibleName("tabbedPane");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTabbedPane jTabbedPane;
    // End of variables declaration//GEN-END:variables
}
