/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.uem.poo.clinica.gui.janelas.painel.secretaria;

import br.com.uem.poo.clinica.entidade.Consulta;
import br.com.uem.poo.clinica.entidade.Contato;
import br.com.uem.poo.clinica.entidade.Paciente;
import br.com.uem.poo.clinica.servicos.Secretaria;
import br.com.uem.poo.clinica.util.DateTimeUtil;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author johnwill
 */
public class PainelRelatorioConsulta extends javax.swing.JPanel {

      /**
     * Creates new form JanelaSecretaria
     */
    public PainelRelatorioConsulta() {
        initComponents();
        secretaria = new Secretaria();
        myCustomizations();
    }
    
    
    private void  myCustomizations(){
            jCPessoa.removeAllItems();
            secretaria.listaPacientes().forEach(p -> {
                jCPessoa.addItem(p.getId()+"-"+p.getNome());
        });
            
           buttonGroup.add(jRCurta);
           buttonGroup.add(jRLongo);
           
           
           buttonGroupOpcoesPesquisa.add(jREmail);
           buttonGroupOpcoesPesquisa.add(jRTelefone);
           
           jRCurta.setSelected(true);
           jRTelefone.setSelected(true);
            
            criaTabela();
            carregaTabela();
    }
    
   
    
   private void criaTabela(){
        modelTabelaPessoas =new DefaultTableModel(new Object[][]{}, new String[]{
            "ID","Nome", "Tipo", "Horario", "Medico"
        });
        tabelaPacientes.setModel(modelTabelaPessoas);
    }
    
    private void carregaTabela(){
        List<Consulta> consultas = secretaria.listaConsulta();
        completaTabela(consultas);
        
    }
    
    
    private void completaTabela(List<Consulta> consultas){
       limpaTabela();
       for(Consulta c:consultas){
           String dataHora = DateTimeUtil.converteLocalDateTimeParaString(c.getDiaHorario(), "dd/MM/yyyy - HH:mm");
            modelTabelaPessoas.addRow(new Object[]{
               c.getId(),
                c.getPaciente().getNome(),
                c.getDuracao()==60?"Longa":"Curta",
                dataHora,
                c.getNomeMedico()
            });
        }
    }
   private void limpaTabela(){
        int length=modelTabelaPessoas.getRowCount();
        for(int i=0;i<length;i++){
            modelTabelaPessoas.removeRow(0);
            
        }
    }
   private void selecionaRow(){
        if(tabelaPacientes.getSelectedRow()!=-1){
            int row=tabelaPacientes.getSelectedRow();
            String idString = tabelaPacientes.getValueAt(row, 0).toString();
            Long id = Long.parseLong(idString);
            Consulta c =secretaria.buscaConsultaPeloId(id);
            
            jTId.setText(c.getId().toString());
            if(c.getDuracao()==60){
               jRLongo.setSelected(true);
            }else{
                jRCurta.setSelected(true);
            }
            
            for(int i =0; i<jCPessoa.getItemCount();i++){
                
                 String str = jCPessoa.getItemAt(i);
                 if(str.contains(c.getPaciente().getNome())){
                     jCPessoa.setSelectedIndex(i);
                     break;
                 }
            }
            
            
            jTMedico.setText(c.getNomeMedico());
            jTDataHorario.setText(DateTimeUtil.converteLocalDateTimeParaString(c.getDiaHorario(), "dd/MM/yyyy - HH:mm"));
        
       }
    }
    
   private  Consulta capturaConsulta(){
       
        Consulta.ConsultaBuild cb = new Consulta.ConsultaBuild();
        LocalDateTime ldt = DateTimeUtil.converteStringParaLocalDateTime(jTDataHorario.getText(), "dd/MM/yyyy - HH:mm");
        String pessoaString = jCPessoa.getSelectedItem().toString();
        String idPAcienteString = pessoaString.substring(0,pessoaString.indexOf("-"));
        int duracao;
        if(jRLongo.isSelected()){
            duracao=60;
        }else{
            duracao=30;
        }
        
        
        Long id = jTId.getText().isEmpty()?null:Long.parseLong(jTId.getText()); 
        
        Consulta consulta = cb.id(id)
                .paciente(secretaria.buscaPacientePeloId(Long.parseLong(idPAcienteString)))
                .diaHorario(ldt)
                .duracao(duracao)
                .nomeMedico(jTMedico.getText())
                .build();
        
        return consulta;    
    }
    
    private void limpaCampos(){
         jTId.setText("");
         jTDataHorario.setText("");
         jTDataHorario.setText("");
         jRCurta.setSelected(true);
         jTMedico.setText("");
         jCPessoa.setSelectedIndex(0);
    }

    private void buscaTelefoneOuEmail(){
        List<Consulta> consultas;
        if(jRTelefone.isSelected()){
            consultas = secretaria.listaConsultaPeloTelefone(jTPesquisa.getText());
        }else{
            consultas = secretaria.listaConsultaPeloEmail(jTPesquisa.getText());
        }
        limpaTabela();
        completaTabela(consultas);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup = new javax.swing.ButtonGroup();
        buttonGroupOpcoesPesquisa = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        painelDadosPaciente = new javax.swing.JPanel();
        dados = new javax.swing.JPanel();
        jLId = new javax.swing.JLabel();
        jTId = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jCPessoa = new javax.swing.JComboBox<>();
        jTMedico = new javax.swing.JTextField();
        jTDataHorario = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jRLongo = new javax.swing.JRadioButton();
        jRCurta = new javax.swing.JRadioButton();
        painelDados = new javax.swing.JPanel();
        jCProximoDia = new javax.swing.JCheckBox();
        jLabel3 = new javax.swing.JLabel();
        jTDataPesquisa = new javax.swing.JTextField();
        jREmail = new javax.swing.JRadioButton();
        jRTelefone = new javax.swing.JRadioButton();
        jTPesquisa = new javax.swing.JTextField();
        jBBuscarPorEmailOuTelefone = new javax.swing.JButton();
        jBBuscarPelaData = new javax.swing.JButton();
        painelOp = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaPacientes = new javax.swing.JTable();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        painelDadosPaciente.setBackground(new java.awt.Color(255, 255, 255));
        painelDadosPaciente.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados Paciente"));
        painelDadosPaciente.setLayout(new java.awt.BorderLayout());

        dados.setBackground(new java.awt.Color(255, 255, 255));

        jLId.setText("ID");

        jTId.setEnabled(false);

        jLabel2.setText("Paciente");

        jLabel1.setText("Nome medico");

        jCPessoa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCPessoa.setToolTipText("");
        jCPessoa.setEnabled(false);
        jCPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCPessoaActionPerformed(evt);
            }
        });

        jTMedico.setEnabled(false);

        jTDataHorario.setEnabled(false);
        jTDataHorario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTDataHorarioActionPerformed(evt);
            }
        });

        jLabel11.setText("Data horario");

        jLabel12.setText("Tipo de consulta");

        jRLongo.setText("Longa");
        jRLongo.setEnabled(false);
        jRLongo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRLongoActionPerformed(evt);
            }
        });

        jRCurta.setText("Curta");
        jRCurta.setEnabled(false);
        jRCurta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRCurtaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dadosLayout = new javax.swing.GroupLayout(dados);
        dados.setLayout(dadosLayout);
        dadosLayout.setHorizontalGroup(
            dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dadosLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addComponent(jLId)
                        .addGap(40, 40, 40)
                        .addComponent(jTId, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 372, Short.MAX_VALUE)
                        .addComponent(jLabel1))
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel11))
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(18, 18, 18)
                        .addComponent(jRCurta)
                        .addGap(26, 26, 26)
                        .addComponent(jRLongo)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(34, 34, 34)
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTMedico, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTDataHorario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(66, 66, 66))
        );
        dadosLayout.setVerticalGroup(
            dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dadosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLId)
                    .addComponent(jTId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jTMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jCPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(jTDataHorario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jRLongo)
                    .addComponent(jRCurta)))
        );

        painelDadosPaciente.add(dados, java.awt.BorderLayout.PAGE_START);

        painelDados.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jCProximoDia.setSelected(true);
        jCProximoDia.setText("Consulta proximo dia");
        jCProximoDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCProximoDiaActionPerformed(evt);
            }
        });
        painelDados.add(jCProximoDia, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 40, -1, -1));

        jLabel3.setText("Digite a data para pesquisa");
        painelDados.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 10, -1, -1));

        jTDataPesquisa.setEnabled(false);
        painelDados.add(jTDataPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 10, 120, -1));

        jREmail.setText("Email");
        painelDados.add(jREmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, -1, -1));

        jRTelefone.setText("Telefone");
        painelDados.add(jRTelefone, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, -1, -1));

        jTPesquisa.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                jTPesquisaCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            }
        });
        jTPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTPesquisaKeyReleased(evt);
            }
        });
        painelDados.add(jTPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 300, -1));

        jBBuscarPorEmailOuTelefone.setText("buscar");
        jBBuscarPorEmailOuTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarPorEmailOuTelefoneActionPerformed(evt);
            }
        });
        painelDados.add(jBBuscarPorEmailOuTelefone, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 50, -1, -1));

        jBBuscarPelaData.setText("Buscar");
        jBBuscarPelaData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarPelaDataActionPerformed(evt);
            }
        });
        painelDados.add(jBBuscarPelaData, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 40, -1, -1));

        painelOp.setBackground(new java.awt.Color(255, 255, 255));
        painelOp.setBorder(javax.swing.BorderFactory.createTitledBorder("Tabela Pacientes"));
        painelOp.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jScrollPane1KeyPressed(evt);
            }
        });

        tabelaPacientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelaPacientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaPacientesMouseClicked(evt);
            }
        });
        tabelaPacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tabelaPacientesKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaPacientes);

        painelOp.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 830, 130));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(painelDadosPaciente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(painelDados, javax.swing.GroupLayout.DEFAULT_SIZE, 833, Short.MAX_VALUE)
                    .addComponent(painelOp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(painelDadosPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelDados, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelOp, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 15, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void jCPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCPessoaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCPessoaActionPerformed

    private void tabelaPacientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaPacientesMouseClicked
        selecionaRow();
    }//GEN-LAST:event_tabelaPacientesMouseClicked

    private void jScrollPane1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jScrollPane1KeyPressed
        selecionaRow();
    }//GEN-LAST:event_jScrollPane1KeyPressed

    private void jTDataHorarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTDataHorarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTDataHorarioActionPerformed

    private void jRLongoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRLongoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRLongoActionPerformed

    private void jRCurtaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRCurtaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRCurtaActionPerformed

    private void tabelaPacientesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaPacientesKeyReleased
        selecionaRow();
    }//GEN-LAST:event_tabelaPacientesKeyReleased

    private void jCProximoDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCProximoDiaActionPerformed
       if(!jCProximoDia.isSelected()){
           jTDataPesquisa.setEnabled(true);
       }else{
           jTDataPesquisa.setEnabled(false);
       }
    }//GEN-LAST:event_jCProximoDiaActionPerformed

    private void jTPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTPesquisaKeyReleased
        buscaTelefoneOuEmail();       
    }//GEN-LAST:event_jTPesquisaKeyReleased

    private void jTPesquisaCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jTPesquisaCaretPositionChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_jTPesquisaCaretPositionChanged

    private void jBBuscarPorEmailOuTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarPorEmailOuTelefoneActionPerformed
        buscaTelefoneOuEmail();
    }//GEN-LAST:event_jBBuscarPorEmailOuTelefoneActionPerformed

    private void jBBuscarPelaDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarPelaDataActionPerformed
        try{
            LocalDate ld;
            if(jCProximoDia.isSelected()){
                ld = LocalDate.now();
                ld= ld.plusDays(1);
            }else{
                ld = DateTimeUtil.converteStringParaLocalDate(jTDataPesquisa.getText(), "dd/MM/yyyy");
            }
             List<Consulta> consultas;
            consultas = secretaria.listaConsultaPelaData(ld);
            
            completaTabela(consultas);
       }catch(Exception ex){
           JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
           throw new RuntimeException(ex);
       }
    }//GEN-LAST:event_jBBuscarPelaDataActionPerformed

private  DefaultTableModel modelTabelaPessoas;
    private Secretaria secretaria;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.ButtonGroup buttonGroupOpcoesPesquisa;
    private javax.swing.JPanel dados;
    private javax.swing.JButton jBBuscarPelaData;
    private javax.swing.JButton jBBuscarPorEmailOuTelefone;
    private javax.swing.JComboBox<String> jCPessoa;
    private javax.swing.JCheckBox jCProximoDia;
    private javax.swing.JLabel jLId;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRCurta;
    private javax.swing.JRadioButton jREmail;
    private javax.swing.JRadioButton jRLongo;
    private javax.swing.JRadioButton jRTelefone;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTDataHorario;
    private javax.swing.JTextField jTDataPesquisa;
    private javax.swing.JTextField jTId;
    private javax.swing.JTextField jTMedico;
    private javax.swing.JTextField jTPesquisa;
    private javax.swing.JPanel painelDados;
    private javax.swing.JPanel painelDadosPaciente;
    private javax.swing.JPanel painelOp;
    private javax.swing.JTable tabelaPacientes;
    // End of variables declaration//GEN-END:variables
}
