/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.uem.poo.clinica.gui.janelas.painel.medico;

import br.com.uem.poo.clinica.gui.janelas.painel.secretaria.*;
import br.com.uem.poo.clinica.entidade.Consulta;
import br.com.uem.poo.clinica.entidade.Contato;
import br.com.uem.poo.clinica.entidade.DadosAdicionaisPaciente;
import br.com.uem.poo.clinica.entidade.Paciente;
import br.com.uem.poo.clinica.servicos.Medico;
import br.com.uem.poo.clinica.servicos.Secretaria;
import br.com.uem.poo.clinica.util.DateTimeUtil;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;
import org.hibernate.dialect.DB2390Dialect;

/**
 *
 * @author johnwill
 */
public class PainelDadosAdicionais extends javax.swing.JPanel {

      /**
     * Creates new form JanelaSecretaria
     */
    public PainelDadosAdicionais() {
        initComponents();
        medico = new Medico();
        myCustomizations();
    }
    
    
   private  void  myCustomizations(){
       jCPessoa.removeAllItems();
       
       if(medico.listaPacientes().size()==0){
           throw new RuntimeException("Lista Pacientes vazia");
       }
       
        medico.listaPacientes().forEach(p -> {
                jCPessoa.addItem(p.getId()+"-"+p.getNome());
        });
        criaTabela();
        carregaTabela();
    }
    
   
    
    private  void criaTabela(){
        modelTabelaPessoas =new DefaultTableModel(new Object[][]{}, new String[]{
            "ID","Dado adicional"
        });
        tabelaPacientes.setModel(modelTabelaPessoas);
    }
    
    private  void carregaTabela(){
        List<DadosAdicionaisPaciente> dap;
        Long id = getIdPacienteSelecionado();
        if(id!=null){
            Paciente paciente = medico.buscaPacientePeloId(id);
            dap = paciente.getDadosAdicionais();
        }else{
            dap = List.of();
        }
        completaTabela(dap);
    }
    
    
    private  void completaTabela( List<DadosAdicionaisPaciente> dap){
       limpaTabela();
       for(DadosAdicionaisPaciente d:dap){
            modelTabelaPessoas.addRow(new Object[]{
               d.getId(),
               d.getDadoAdicional()
            });
        }
    }
    private  void limpaTabela(){
        int length=modelTabelaPessoas.getRowCount();
        for(int i=0;i<length;i++){
            modelTabelaPessoas.removeRow(0);
            
        }
    }
    private  void selecionaRow(){
        if(tabelaPacientes.getSelectedRow()!=-1){
            int row=tabelaPacientes.getSelectedRow();
            String idString = tabelaPacientes.getValueAt(row, 0).toString();
            Long id = Long.parseLong(idString);
           
            DadosAdicionaisPaciente dap = medico.buscaDadoPeloId(id);
            
            jTId.setText(dap.getId().toString());
            jTextArea.setText(dap.getDadoAdicional());
       }
    }
    
    private DadosAdicionaisPaciente capturaDado(){
        Long id;
        if(jTId.getText().equals("")){
            id = null;
        }else{
            id = Long.parseLong(jTId.getText());
        }
        String dado = jTextArea.getText();
        Paciente p = medico.buscaPacientePeloId(getIdPacienteSelecionado());
        DadosAdicionaisPaciente dap = new DadosAdicionaisPaciente(id, p, dado);
        return dap;
    }
    
    
    
    private void limpaCampos(){
         jTId.setText("");
        jTextArea.setText("");
    }
    
    private Long getIdPacienteSelecionado(){
        String pessoaString = jCPessoa.getSelectedItem().toString();
         String idPacienteString = pessoaString.substring(0,pessoaString.indexOf("-"));
        Long id = Long.parseLong(idPacienteString);
        return id;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        painelDadosPaciente = new javax.swing.JPanel();
        dados = new javax.swing.JPanel();
        jLId = new javax.swing.JLabel();
        jTId = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jCPessoa = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea = new javax.swing.JTextArea();
        painelDados = new javax.swing.JPanel();
        painelOperacoes = new javax.swing.JPanel();
        jBSalvar = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBLimpar = new javax.swing.JButton();
        painelOp = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaPacientes = new javax.swing.JTable();
        jBAtualizaTab = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        painelDadosPaciente.setBackground(new java.awt.Color(255, 255, 255));
        painelDadosPaciente.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados Paciente"));
        painelDadosPaciente.setLayout(new java.awt.BorderLayout());

        dados.setBackground(new java.awt.Color(255, 255, 255));

        jLId.setText("ID");

        jTId.setEnabled(false);

        jLabel2.setText("Paciente");

        jCPessoa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCPessoa.setToolTipText("");
        jCPessoa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jCPessoaMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jCPessoaMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jCPessoaMouseReleased(evt);
            }
        });
        jCPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCPessoaActionPerformed(evt);
            }
        });
        jCPessoa.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jCPessoaPropertyChange(evt);
            }
        });

        jLabel4.setText("Informação:");

        javax.swing.GroupLayout dadosLayout = new javax.swing.GroupLayout(dados);
        dados.setLayout(dadosLayout);
        dadosLayout.setHorizontalGroup(
            dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dadosLayout.createSequentialGroup()
                .addContainerGap(42, Short.MAX_VALUE)
                .addComponent(jLId)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTId, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(159, 159, 159)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(247, 247, 247))
            .addGroup(dadosLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        dadosLayout.setVerticalGroup(
            dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dadosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jTId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLId))
                .addGap(37, 37, 37)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        painelDadosPaciente.add(dados, java.awt.BorderLayout.PAGE_START);

        jTextArea.setColumns(20);
        jTextArea.setRows(5);
        jScrollPane2.setViewportView(jTextArea);

        painelDadosPaciente.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        painelDados.setLayout(new java.awt.BorderLayout());

        painelOperacoes.setBackground(new java.awt.Color(255, 255, 255));
        painelOperacoes.setBorder(javax.swing.BorderFactory.createTitledBorder("Operacoes"));

        jBSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/novo-foco.png"))); // NOI18N
        jBSalvar.setText("Salvar");
        jBSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSalvarActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBSalvar);

        jBAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/salvar-foco.png"))); // NOI18N
        jBAlterar.setText("Alterar");
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBAlterar);

        jBExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/excluir-foco.png"))); // NOI18N
        jBExcluir.setText("Excluir");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBExcluir);

        jBLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/apagador.png"))); // NOI18N
        jBLimpar.setText("Limpar");
        jBLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLimparActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBLimpar);

        painelDados.add(painelOperacoes, java.awt.BorderLayout.CENTER);

        painelOp.setBackground(new java.awt.Color(255, 255, 255));
        painelOp.setBorder(javax.swing.BorderFactory.createTitledBorder("Tabela Dados adicionais"));
        painelOp.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jScrollPane1KeyPressed(evt);
            }
        });

        tabelaPacientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelaPacientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaPacientesMouseClicked(evt);
            }
        });
        tabelaPacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tabelaPacientesKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaPacientes);

        painelOp.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 830, 130));

        jBAtualizaTab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/refresh.png"))); // NOI18N
        jBAtualizaTab.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jBAtualizaTab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAtualizaTabActionPerformed(evt);
            }
        });
        painelOp.add(jBAtualizaTab, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 10, -1, -1));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(painelDadosPaciente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(painelDados, javax.swing.GroupLayout.DEFAULT_SIZE, 833, Short.MAX_VALUE)
                    .addComponent(painelOp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(painelDadosPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelDados, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelOp, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 15, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void jCPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCPessoaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCPessoaActionPerformed

    private void tabelaPacientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaPacientesMouseClicked
        selecionaRow();
    }//GEN-LAST:event_tabelaPacientesMouseClicked

    private void jScrollPane1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jScrollPane1KeyPressed
        selecionaRow();
    }//GEN-LAST:event_jScrollPane1KeyPressed

    private void jBSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSalvarActionPerformed
        try{
            DadosAdicionaisPaciente c = capturaDado();
            medico.adicionaDadosAdicional(c);
            carregaTabela();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBSalvarActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
       try{
            if(jTId.getText().equals("")){
                JOptionPane.showMessageDialog(null, "selecione um registro", "Erro", JOptionPane.ERROR_MESSAGE);

            }else{
                 DadosAdicionaisPaciente c = capturaDado();
                 medico.atualizaDadosAdicional(c);
                 carregaTabela();
            }
            limpaCampos();
         }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
       try{
            Long id = jTId.getText().isEmpty()?null:Long.parseLong(jTId.getText());
            if(id==null){
                JOptionPane.showMessageDialog(null, "selecione um registro", "Erro", JOptionPane.ERROR_MESSAGE);

            }else{
                medico.removeDadosAdicional(id);
                carregaTabela();
            }
            limpaCampos();
       } catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLimparActionPerformed
        limpaCampos();
        carregaTabela();
    }//GEN-LAST:event_jBLimparActionPerformed

    private void tabelaPacientesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaPacientesKeyReleased
        selecionaRow();
    }//GEN-LAST:event_tabelaPacientesKeyReleased

    private void jCPessoaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCPessoaMouseReleased

        carregaTabela();
    }//GEN-LAST:event_jCPessoaMouseReleased

    private void jCPessoaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCPessoaMouseClicked
       
    }//GEN-LAST:event_jCPessoaMouseClicked

    private void jCPessoaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCPessoaMousePressed
         limpaCampos();
        carregaTabela();
    }//GEN-LAST:event_jCPessoaMousePressed

    private void jBAtualizaTabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAtualizaTabActionPerformed
       limpaCampos();
        carregaTabela();
    }//GEN-LAST:event_jBAtualizaTabActionPerformed

    private void jCPessoaPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jCPessoaPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jCPessoaPropertyChange

    private  DefaultTableModel modelTabelaPessoas;
    private Medico medico;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JPanel dados;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBAtualizaTab;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBLimpar;
    private javax.swing.JButton jBSalvar;
    private javax.swing.JComboBox<String> jCPessoa;
    private javax.swing.JLabel jLId;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTId;
    private javax.swing.JTextArea jTextArea;
    private javax.swing.JPanel painelDados;
    private javax.swing.JPanel painelDadosPaciente;
    private javax.swing.JPanel painelOp;
    private javax.swing.JPanel painelOperacoes;
    private javax.swing.JTable tabelaPacientes;
    // End of variables declaration//GEN-END:variables
}
