/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.uem.poo.clinica.gui.janelas.painel.secretaria;

import br.com.uem.poo.clinica.entidade.Contato;
import br.com.uem.poo.clinica.entidade.Paciente;
import br.com.uem.poo.clinica.servicos.Secretaria;
import br.com.uem.poo.clinica.util.DateTimeUtil;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author johnwill
 */
public class PainelPaciente extends javax.swing.JPanel {

      /**
     * Creates new form JanelaSecretaria
     */
    public PainelPaciente() {
        initComponents();
        secretaria = new Secretaria();
        myCustomizations();
        carregaTabela();
    }
    
    
   private  void  myCustomizations(){
            jCSexo.removeAllItems();
            jCSexo.addItem("Feminino");
            jCSexo.addItem("Masculino");
            jCSexo.addItem("Outros");
            
            jCEstadoCivil.removeAllItems();
            jCEstadoCivil.addItem("Casado");
            jCEstadoCivil.addItem("Solteiro");
            jCEstadoCivil.addItem("Outro");
            
            jCConvenio.removeAllItems();
            jCConvenio.addItem("Particular");
            jCConvenio.addItem("Publico");
            
            try {
               MaskFormatter formatterData = new MaskFormatter("##/##/####");
               MaskFormatter formatterTelefone = new MaskFormatter("(##) # ####-####");
               
               formatterData.install(jFDataNascimento);
               formatterTelefone.install(jFTelefone);
             } catch (ParseException ex) {
                 throw new RuntimeException(ex);
             }
            
            criaTabela();
            carregaTabela();
    }
    
    private MaskFormatter geraMascaraParaCampo(String mask){
       MaskFormatter maskFormatter;
       try {
           maskFormatter = new MaskFormatter(mask);
           maskFormatter.setPlaceholderCharacter('_');
        } catch (ParseException ex) {
            throw new RuntimeException(ex);
        }
       return maskFormatter;
    }
    
    private  void criaTabela(){
        modelTabelaPessoas =new DefaultTableModel(new Object[][]{}, new String[]{
            "ID","Nome", "E-Mail", "Telefone"
        });
        tabelaPacientes.setModel(modelTabelaPessoas);
    }
    
    private  void carregaTabela(){
        List<Paciente> pacientes = secretaria.listaPacientes();
        completaTabela(pacientes);
        
    }
    
    private  void carregaTabela(String str){
        List<Paciente> pacientes = secretaria.buscaPacientePeloNome(str);
        completaTabela(pacientes);
    }
    
    private  void completaTabela(List<Paciente> pacientes){
       limpaTabela();
       for(Paciente p:pacientes){
            modelTabelaPessoas.addRow(new Object[]{
                p.getId(),
                p.getNome(),
                p.getContato().getEmail(),
                p.getContato().getTelefone()
            });
        }
    }
    
    private  void limpaTabela(){
        int length=modelTabelaPessoas.getRowCount();
        for(int i=0;i<length;i++){
            modelTabelaPessoas.removeRow(0);
            
        }
    }
    
    private  void selecionaRow(){
        if(tabelaPacientes.getSelectedRow()!=-1){
            int row=tabelaPacientes.getSelectedRow();
            String idString = tabelaPacientes.getValueAt(row, 0).toString();
            Long id = Long.parseLong(idString);
            Paciente p =secretaria.buscaPacientePeloId(id);
            
            jTId.setText(p.getId().toString());
            
             jTBairro.setText(p.getContato().getBairro());
             jTCidade.setText(p.getContato().getCidade());
             jTEmail.setText(p.getContato().getEmail());
             jFTelefone.setText(p.getContato().getTelefone());
             jTEndereco.setText(p.getContato().getEndereco());
             
             jFDataNascimento.setText(DateTimeUtil.converteLocalDateParaString(p.getDataNascimento(), "dd/MM/yyyy"));
             
             jTNome.setText(p.getNome());
             
             jCEstadoCivil.setSelectedItem(p.getEstadoCivil());
             
             jCSexo.setSelectedItem(p.getSexo());
             
             jCConvenio.setSelectedItem(p.getTipoConvenio());
            
        }
    }
    
   private  Paciente capturaPaciente(){
        Paciente.PacienteBuilder pb = new Paciente.PacienteBuilder();
        Contato.ContatoBuilder cb = new Contato.ContatoBuilder();
        LocalDate ld = DateTimeUtil.converteStringParaLocalDate(jFDataNascimento.getText(), "dd/MM/yyyy");
        String estadoCivil = jCEstadoCivil.getSelectedItem().toString();
        String sexo = jCSexo.getSelectedItem().toString();
        String convenio = jCConvenio.getSelectedItem().toString();
        
        Contato contato = cb.bairro(jTBairro.getText())
                .cidade(jTCidade.getText())
                .email(jTEmail.getText())
                .telefone(jFTelefone.getText())
                .endereco(jTEndereco.getText())
                .build();
        
        Long id = jTId.getText().isEmpty()?null:Long.parseLong(jTId.getText()); 
        
        Paciente p =pb.id(id)
                .contato(contato)
                .dataNascimento(ld)
                .estadoCivil(estadoCivil)
                .sexo(sexo)
                .nome(jTNome.getText())
                .tipoConvenio(convenio)
                .build();
        
        return p;        
    }
    
    private void limpaCampos(){
         jTId.setText("");
            
        jTBairro.setText("");
        jTCidade.setText("");
        jTEmail.setText("");
        jFTelefone.setText("");
        jTEndereco.setText("");


        jTEmail.setText("");
        jTNome.setText("");
        jFDataNascimento.setText("");

        jCEstadoCivil.setSelectedIndex(0);

        jCSexo.setSelectedIndex(0);
        jCConvenio.setSelectedIndex(0);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        painelDadosPaciente = new javax.swing.JPanel();
        contato = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jTEndereco = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTEmail = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTCidade = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTBairro = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jFTelefone = new javax.swing.JFormattedTextField();
        dados = new javax.swing.JPanel();
        jLId = new javax.swing.JLabel();
        jTId = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTNome = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jCSexo = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jCEstadoCivil = new javax.swing.JComboBox<>();
        jFDataNascimento = new javax.swing.JFormattedTextField();
        jLabel11 = new javax.swing.JLabel();
        jCConvenio = new javax.swing.JComboBox<>();
        painelDados = new javax.swing.JPanel();
        painelOperacoes = new javax.swing.JPanel();
        jBSalvar = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBLimpar = new javax.swing.JButton();
        painelOp = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaPacientes = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();
        jTextPesquisa = new javax.swing.JTextField();

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        painelDadosPaciente.setBackground(new java.awt.Color(255, 255, 255));
        painelDadosPaciente.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados Paciente"));
        painelDadosPaciente.setLayout(new java.awt.BorderLayout());

        contato.setBackground(new java.awt.Color(255, 255, 255));
        contato.setBorder(javax.swing.BorderFactory.createTitledBorder("Contato"));

        jLabel5.setText("Endereço");

        jTEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTEnderecoActionPerformed(evt);
            }
        });

        jLabel6.setText("Bairro");

        jTEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTEmailActionPerformed(evt);
            }
        });

        jLabel7.setText("Cidade");

        jTCidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTCidadeActionPerformed(evt);
            }
        });

        jLabel8.setText("Email:");

        jTBairro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTBairroActionPerformed(evt);
            }
        });

        jLabel9.setText("Telefone");

        javax.swing.GroupLayout contatoLayout = new javax.swing.GroupLayout(contato);
        contato.setLayout(contatoLayout);
        contatoLayout.setHorizontalGroup(
            contatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contatoLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(contatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(contatoLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(contatoLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(47, 47, 47)
                .addGroup(contatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(contatoLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(56, 56, 56)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jFTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(contatoLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(97, Short.MAX_VALUE))
        );
        contatoLayout.setVerticalGroup(
            contatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contatoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(contatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jTCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jFTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(contatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jTBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        painelDadosPaciente.add(contato, java.awt.BorderLayout.CENTER);

        dados.setBackground(new java.awt.Color(255, 255, 255));

        jLId.setText("ID");

        jTId.setEnabled(false);

        jLabel2.setText("Nome");

        jTNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTNomeActionPerformed(evt);
            }
        });

        jLabel1.setText("Data Nascimento");

        jLabel3.setText("Sexo");

        jCSexo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCSexo.setToolTipText("");
        jCSexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCSexoActionPerformed(evt);
            }
        });

        jLabel4.setText("Estado Civil");

        jCEstadoCivil.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jFDataNascimento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT))));
        jFDataNascimento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFDataNascimentoActionPerformed(evt);
            }
        });

        jLabel11.setText("Tipo de Convenio");

        jCConvenio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCConvenio.setToolTipText("");
        jCConvenio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCConvenioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dadosLayout = new javax.swing.GroupLayout(dados);
        dados.setLayout(dadosLayout);
        dadosLayout.setHorizontalGroup(
            dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dadosLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(dadosLayout.createSequentialGroup()
                                .addComponent(jLId)
                                .addGap(40, 40, 40)
                                .addComponent(jTId, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(dadosLayout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTNome, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)))
                        .addGap(189, 189, 189)
                        .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(dadosLayout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jCSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel1)))
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCConvenio, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(337, 337, 337)))
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jLabel4)
                        .addGap(36, 36, 36)
                        .addComponent(jCEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jFDataNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        dadosLayout.setVerticalGroup(
            dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dadosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLId)
                    .addComponent(jTId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jFDataNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jCSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jCEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jCConvenio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        painelDadosPaciente.add(dados, java.awt.BorderLayout.PAGE_START);

        jPanel1.add(painelDadosPaciente, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 860, 250));

        painelOperacoes.setBackground(new java.awt.Color(255, 255, 255));
        painelOperacoes.setBorder(javax.swing.BorderFactory.createTitledBorder("Operacoes"));

        jBSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/novo-foco.png"))); // NOI18N
        jBSalvar.setText("Salvar");
        jBSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSalvarActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBSalvar);

        jBAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/salvar-foco.png"))); // NOI18N
        jBAlterar.setText("Alterar");
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBAlterar);

        jBExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/excluir-foco.png"))); // NOI18N
        jBExcluir.setText("Excluir");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBExcluir);

        jBLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/apagador.png"))); // NOI18N
        jBLimpar.setText("Limpar");
        jBLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLimparActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBLimpar);

        javax.swing.GroupLayout painelDadosLayout = new javax.swing.GroupLayout(painelDados);
        painelDados.setLayout(painelDadosLayout);
        painelDadosLayout.setHorizontalGroup(
            painelDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelDadosLayout.createSequentialGroup()
                .addComponent(painelOperacoes, javax.swing.GroupLayout.DEFAULT_SIZE, 888, Short.MAX_VALUE)
                .addContainerGap())
        );
        painelDadosLayout.setVerticalGroup(
            painelDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelDadosLayout.createSequentialGroup()
                .addGap(0, 12, Short.MAX_VALUE)
                .addComponent(painelOperacoes, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(painelDados, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 260, 900, 75));

        painelOp.setBackground(new java.awt.Color(255, 255, 255));
        painelOp.setBorder(javax.swing.BorderFactory.createTitledBorder("Tabela Pacientes"));
        painelOp.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jScrollPane1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jScrollPane1KeyReleased(evt);
            }
        });

        tabelaPacientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelaPacientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaPacientesMouseClicked(evt);
            }
        });
        tabelaPacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tabelaPacientesKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaPacientes);

        painelOp.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 820, 130));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/buscar-foco.png"))); // NOI18N
        painelOp.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 20, 30, 20));

        jTextPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextPesquisaActionPerformed(evt);
            }
        });
        jTextPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextPesquisaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextPesquisaKeyReleased(evt);
            }
        });
        painelOp.add(jTextPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 20, 500, -1));

        jPanel1.add(painelOp, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 860, 180));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 924, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 900, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 554, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTEnderecoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTEnderecoActionPerformed

    private void jTEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTEmailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTEmailActionPerformed

    private void jTCidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTCidadeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTCidadeActionPerformed

    private void jTBairroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTBairroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTBairroActionPerformed

    private void jCSexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCSexoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCSexoActionPerformed

    private void tabelaPacientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaPacientesMouseClicked
        selecionaRow();
    }//GEN-LAST:event_tabelaPacientesMouseClicked

    private void jScrollPane1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jScrollPane1KeyPressed
        selecionaRow();
    }//GEN-LAST:event_jScrollPane1KeyPressed

    private void jTextPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextPesquisaActionPerformed

    }//GEN-LAST:event_jTextPesquisaActionPerformed

    private void jTextPesquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextPesquisaKeyPressed

    }//GEN-LAST:event_jTextPesquisaKeyPressed

    private void jTextPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextPesquisaKeyReleased
        carregaTabela(jTextPesquisa.getText());
    }//GEN-LAST:event_jTextPesquisaKeyReleased

    private void jBSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSalvarActionPerformed
        try{
            Paciente p = capturaPaciente();
            secretaria.adicionaPaciente(p);
            limpaCampos();
            carregaTabela();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBSalvarActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
       try{
            if(jTId.getText().equals("")){
                JOptionPane.showMessageDialog(null, "selecione um registro", "Erro", JOptionPane.ERROR_MESSAGE);

            }else{
                Paciente p = capturaPaciente();
                secretaria.atualizaPaciente(p);
                carregaTabela();
            }
            limpaCampos();
         }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
       try{
            Long id = jTId.getText().isEmpty()?null:Long.parseLong(jTId.getText());
            if(id==null){
                JOptionPane.showMessageDialog(null, "selecione um registro", "Erro", JOptionPane.ERROR_MESSAGE);

            }else{
                secretaria.removePaciente(id);
                carregaTabela();
            }
            limpaCampos();
       } catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLimparActionPerformed
        limpaCampos();
    }//GEN-LAST:event_jBLimparActionPerformed

    private void jTNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTNomeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTNomeActionPerformed

    private void jScrollPane1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jScrollPane1KeyReleased
        
    }//GEN-LAST:event_jScrollPane1KeyReleased

    private void tabelaPacientesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaPacientesKeyReleased
      selecionaRow();
    }//GEN-LAST:event_tabelaPacientesKeyReleased

    private void jFDataNascimentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFDataNascimentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jFDataNascimentoActionPerformed

    private void jCConvenioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCConvenioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCConvenioActionPerformed

    private  DefaultTableModel modelTabelaPessoas;
    private Secretaria secretaria;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel contato;
    private javax.swing.JPanel dados;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBLimpar;
    private javax.swing.JButton jBSalvar;
    private javax.swing.JComboBox<String> jCConvenio;
    private javax.swing.JComboBox<String> jCEstadoCivil;
    private javax.swing.JComboBox<String> jCSexo;
    private javax.swing.JFormattedTextField jFDataNascimento;
    private javax.swing.JFormattedTextField jFTelefone;
    private javax.swing.JLabel jLId;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTBairro;
    private javax.swing.JTextField jTCidade;
    private javax.swing.JTextField jTEmail;
    private javax.swing.JTextField jTEndereco;
    private javax.swing.JTextField jTId;
    private javax.swing.JTextField jTNome;
    private javax.swing.JTextField jTextPesquisa;
    private javax.swing.JPanel painelDados;
    private javax.swing.JPanel painelDadosPaciente;
    private javax.swing.JPanel painelOp;
    private javax.swing.JPanel painelOperacoes;
    private javax.swing.JTable tabelaPacientes;
    // End of variables declaration//GEN-END:variables
}
