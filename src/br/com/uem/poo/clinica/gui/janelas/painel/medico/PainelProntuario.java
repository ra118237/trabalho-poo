/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.uem.poo.clinica.gui.janelas.painel.medico;

import br.com.uem.poo.clinica.gui.janelas.painel.secretaria.*;
import br.com.uem.poo.clinica.entidade.Consulta;
import br.com.uem.poo.clinica.entidade.Contato;
import br.com.uem.poo.clinica.entidade.DadosAdicionaisPaciente;
import br.com.uem.poo.clinica.entidade.Paciente;
import br.com.uem.poo.clinica.entidade.Prontuario;
import br.com.uem.poo.clinica.servicos.Medico;
import br.com.uem.poo.clinica.servicos.Secretaria;
import br.com.uem.poo.clinica.util.DateTimeUtil;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;
import org.hibernate.dialect.DB2390Dialect;

/**
 *
 * @author johnwill
 */
public class PainelProntuario extends javax.swing.JPanel {

      /**
     * Creates new form JanelaSecretaria
     */
    public PainelProntuario() {
        initComponents();
        medico = new Medico();
        myCustomizations();
    }
    
    
   private  void  myCustomizations(){
       jCPessoa.removeAllItems();
       
       if(medico.listaPacientes().size()==0){
           throw new RuntimeException("Lista Pacientes vazia");
       }
       
         medico.listaPacientes().forEach(p -> {
                jCPessoa.addItem(p.getId()+"-"+p.getNome());
        });
         
          try {
               MaskFormatter formatterDataTime = new MaskFormatter("##/##/####");
               
               formatterDataTime.install(jFData);
             } catch (ParseException ex) {
                 throw new RuntimeException(ex);
             }
         
            criaTabela();
            carregaTabela();
    }
    
    private  void criaTabela(){
        modelTabelaPessoas =new DefaultTableModel(new Object[][]{}, new String[]{
            "ID","Paciente", "Medico", "Data"
        });
        tabelaPacientes.setModel(modelTabelaPessoas);
    }
    
    private  void carregaTabela(){
        
         List<Prontuario> prontuarios;
        Long id = getIdPacienteSelecionado();
        if(id!=null){
            Paciente paciente = medico.buscaPacientePeloId(id);
            prontuarios = medico.listaProntuariosDeUmPaciente(paciente);
        }else{
            prontuarios = List.of();
        }
        
        completaTabela(prontuarios);
    }
    
    
    private  void completaTabela(   List<Prontuario>  prontuarios){
       limpaTabela();
       for(Prontuario p:prontuarios){
           String data = DateTimeUtil.converteLocalDateParaString(p.getData(), "dd/MM/yyyy");
            modelTabelaPessoas.addRow(new Object[]{
               p.getId(),
               p.getPaciente().getNome(),
               p.getNomeMedico(),
               data
            });
        }
    }
    private  void limpaTabela(){
        int length=modelTabelaPessoas.getRowCount();
        for(int i=0;i<length;i++){
            modelTabelaPessoas.removeRow(0);
            
        }
    }
    private  void selecionaRow(){
        if(tabelaPacientes.getSelectedRow()!=-1){
            int row=tabelaPacientes.getSelectedRow();
            String idString = tabelaPacientes.getValueAt(row, 0).toString();
            Long id = Long.parseLong(idString);
           
            Prontuario p = medico.buscaProntuarioPeloId(id);
            String data = DateTimeUtil.converteLocalDateParaString(p.getData(), "dd/MM/yyyy");
            
            jTId.setText(p.getId().toString());
            jTNomeMedico.setText(p.getNomeMedico());
            jFData.setText(data);
            jTSintomas.setText(p.getSintomas());
            jTDiagnostico.setText(p.getDiagnosticoDoenca());
            jTratamento.setText(p.getTratamento());
       }
    }
    
    private Prontuario capturaProntuario(){
        Long id;
        if(jTId.getText().equals("")){
            id = null;
        }else{
            id = Long.parseLong(jTId.getText());
        }
        String nomeMedico = jTNomeMedico.getText();
        String sintomas = jTSintomas.getText();
        String diagnostico = jTDiagnostico.getText();
        String tratamento = jTratamento.getText();
        LocalDate ld = DateTimeUtil.converteStringParaLocalDate(jFData.getText(),"dd/MM/yyyy");
        Paciente p = medico.buscaPacientePeloId(getIdPacienteSelecionado());
        
        Prontuario.ProntuarioBuilder prontuarioBuilder = new Prontuario.ProntuarioBuilder();
        prontuarioBuilder.id(id)
                .paciente(p)
                .nomeMedico(nomeMedico)
                .sintomas(sintomas)
                .diagnosticoDoenca(diagnostico)
                .tratamento(tratamento)
                .data(ld); 
        
        return prontuarioBuilder.build();
    }
    
    
    
    private void limpaCampos(){
        jTId.setText("");
        jTNomeMedico.setText("");
        jFData.setText("");
        jTSintomas.setText("");
        jTDiagnostico.setText("");
        jTratamento.setText("");
    }
    
    private Long getIdPacienteSelecionado(){
        String pessoaString = jCPessoa.getSelectedItem().toString();
         String idPacienteString = pessoaString.substring(0,pessoaString.indexOf("-"));
        Long id = Long.parseLong(idPacienteString);
        return id;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup = new javax.swing.ButtonGroup();
        jPRoot = new javax.swing.JPanel();
        painelDadosPaciente = new javax.swing.JPanel();
        dados = new javax.swing.JPanel();
        jLId = new javax.swing.JLabel();
        jTId = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jCPessoa = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jTNomeMedico = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jFData = new javax.swing.JFormattedTextField();
        painelDados = new javax.swing.JPanel();
        painelOp = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaPacientes = new javax.swing.JTable();
        jBAtualizaTab = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTSintomas = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        jTDiagnostico = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTratamento = new javax.swing.JTextArea();
        painelOperacoes = new javax.swing.JPanel();
        jBSalvar = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBLimpar = new javax.swing.JButton();

        painelDadosPaciente.setBackground(new java.awt.Color(255, 255, 255));
        painelDadosPaciente.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados Paciente"));
        painelDadosPaciente.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        dados.setBackground(new java.awt.Color(255, 255, 255));

        jLId.setText("ID");

        jTId.setEnabled(false);

        jLabel2.setText("Paciente");

        jCPessoa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCPessoa.setToolTipText("");
        jCPessoa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jCPessoaMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jCPessoaMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jCPessoaMouseReleased(evt);
            }
        });
        jCPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCPessoaActionPerformed(evt);
            }
        });
        jCPessoa.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jCPessoaPropertyChange(evt);
            }
        });

        jLabel4.setText("Nome medico");

        jLabel1.setText("Data");

        javax.swing.GroupLayout dadosLayout = new javax.swing.GroupLayout(dados);
        dados.setLayout(dadosLayout);
        dadosLayout.setHorizontalGroup(
            dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dadosLayout.createSequentialGroup()
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLId)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTId, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(331, 331, 331))
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTNomeMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addGap(41, 41, 41)
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jFData, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(52, 52, 52))
        );
        dadosLayout.setVerticalGroup(
            dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dadosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jTId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLId))
                .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dadosLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jTNomeMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(20, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dadosLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(dadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jFData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(28, 28, 28))))
        );

        painelDadosPaciente.add(dados, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 17, 820, 110));

        painelDados.setLayout(new java.awt.BorderLayout());

        painelOp.setBackground(new java.awt.Color(255, 255, 255));
        painelOp.setBorder(javax.swing.BorderFactory.createTitledBorder("Tabela Prontuario"));
        painelOp.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jScrollPane1KeyPressed(evt);
            }
        });

        tabelaPacientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelaPacientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaPacientesMouseClicked(evt);
            }
        });
        tabelaPacientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tabelaPacientesKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaPacientes);

        painelOp.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 60, 730, 130));

        jBAtualizaTab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/refresh.png"))); // NOI18N
        jBAtualizaTab.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jBAtualizaTab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAtualizaTabActionPerformed(evt);
            }
        });
        painelOp.add(jBAtualizaTab, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 20, -1, -1));

        painelDados.add(painelOp, java.awt.BorderLayout.CENTER);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Sintomas e tratamento"));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setText("Sintomas Medicos");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        jTSintomas.setColumns(20);
        jTSintomas.setRows(5);
        jScrollPane2.setViewportView(jTSintomas);

        jPanel2.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 20, 500, -1));

        jLabel5.setText("Diagnostico");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));
        jPanel2.add(jTDiagnostico, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 120, 500, -1));

        jLabel6.setText("Tratamento");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, -1));

        jTratamento.setColumns(20);
        jTratamento.setRows(5);
        jScrollPane3.setViewportView(jTratamento);

        jPanel2.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 160, 500, -1));

        painelOperacoes.setBackground(new java.awt.Color(255, 255, 255));
        painelOperacoes.setBorder(javax.swing.BorderFactory.createTitledBorder("Operacoes"));

        jBSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/novo-foco.png"))); // NOI18N
        jBSalvar.setText("Salvar");
        jBSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSalvarActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBSalvar);

        jBAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/salvar-foco.png"))); // NOI18N
        jBAlterar.setText("Alterar");
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBAlterar);

        jBExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/img/excluir-foco.png"))); // NOI18N
        jBExcluir.setText("Excluir");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBExcluir);

        jBLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/midia/apagador.png"))); // NOI18N
        jBLimpar.setText("Limpar");
        jBLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLimparActionPerformed(evt);
            }
        });
        painelOperacoes.add(jBLimpar);

        javax.swing.GroupLayout jPRootLayout = new javax.swing.GroupLayout(jPRoot);
        jPRoot.setLayout(jPRootLayout);
        jPRootLayout.setHorizontalGroup(
            jPRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPRootLayout.createSequentialGroup()
                .addGroup(jPRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(painelOperacoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPRootLayout.createSequentialGroup()
                        .addGroup(jPRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(painelDadosPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 809, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(painelDados, javax.swing.GroupLayout.PREFERRED_SIZE, 815, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPRootLayout.setVerticalGroup(
            jPRootLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPRootLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(painelDadosPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelOperacoes, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelDados, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPRoot, javax.swing.GroupLayout.PREFERRED_SIZE, 816, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jPRoot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPRoot.getAccessibleContext().setAccessibleDescription("");
    }// </editor-fold>//GEN-END:initComponents

    private void jCPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCPessoaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCPessoaActionPerformed

    private void tabelaPacientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaPacientesMouseClicked
        selecionaRow();
    }//GEN-LAST:event_tabelaPacientesMouseClicked

    private void jScrollPane1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jScrollPane1KeyPressed
        selecionaRow();
    }//GEN-LAST:event_jScrollPane1KeyPressed

    private void jBSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSalvarActionPerformed
        try{
            Prontuario p = capturaProntuario();
            medico.adicionaProntuario(p);
            limpaCampos();
            carregaTabela();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBSalvarActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
       try{
            if(jTId.getText().equals("")){
                JOptionPane.showMessageDialog(null, "selecione um registro", "Erro", JOptionPane.ERROR_MESSAGE);

            }else{
                Prontuario p = capturaProntuario();
            medico.atualizaProntuario(p);
                 carregaTabela();
            }
            limpaCampos();
         }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
       try{
            Long id = jTId.getText().isEmpty()?null:Long.parseLong(jTId.getText());
            if(id==null){
                JOptionPane.showMessageDialog(null, "selecione um registro", "Erro", JOptionPane.ERROR_MESSAGE);

            }else{
                medico.removeProntuario(id);
                carregaTabela();
            }
            limpaCampos();
       } catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException(ex);
        }
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLimparActionPerformed
        limpaCampos();
        carregaTabela();
    }//GEN-LAST:event_jBLimparActionPerformed

    private void tabelaPacientesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaPacientesKeyReleased
        selecionaRow();
    }//GEN-LAST:event_tabelaPacientesKeyReleased

    private void jCPessoaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCPessoaMouseReleased

        carregaTabela();
    }//GEN-LAST:event_jCPessoaMouseReleased

    private void jCPessoaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCPessoaMouseClicked
       
    }//GEN-LAST:event_jCPessoaMouseClicked

    private void jCPessoaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCPessoaMousePressed
         limpaCampos();
        carregaTabela();
    }//GEN-LAST:event_jCPessoaMousePressed

    private void jCPessoaPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jCPessoaPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jCPessoaPropertyChange

    private void jBAtualizaTabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAtualizaTabActionPerformed
        limpaCampos();
        carregaTabela();
    }//GEN-LAST:event_jBAtualizaTabActionPerformed

    private  DefaultTableModel modelTabelaPessoas;
    private Medico medico;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JPanel dados;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBAtualizaTab;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBLimpar;
    private javax.swing.JButton jBSalvar;
    private javax.swing.JComboBox<String> jCPessoa;
    private javax.swing.JFormattedTextField jFData;
    private javax.swing.JLabel jLId;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPRoot;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTDiagnostico;
    private javax.swing.JTextField jTId;
    private javax.swing.JTextField jTNomeMedico;
    private javax.swing.JTextArea jTSintomas;
    private javax.swing.JTextArea jTratamento;
    private javax.swing.JPanel painelDados;
    private javax.swing.JPanel painelDadosPaciente;
    private javax.swing.JPanel painelOp;
    private javax.swing.JPanel painelOperacoes;
    private javax.swing.JTable tabelaPacientes;
    // End of variables declaration//GEN-END:variables
}
