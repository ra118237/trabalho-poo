package br.com.uem.poo.clinica.servicos;

import br.com.uem.poo.clinica.dao.DadosAdicionaisDaoImpl;
import br.com.uem.poo.clinica.dao.PacienteDaoImpl;
import br.com.uem.poo.clinica.dao.ProntuarioDaoImpl;
import br.com.uem.poo.clinica.entidade.Consulta;
import br.com.uem.poo.clinica.entidade.DadosAdicionaisPaciente;
import br.com.uem.poo.clinica.entidade.Paciente;
import br.com.uem.poo.clinica.entidade.Prontuario;
import br.com.uem.poo.clinica.repository.DadosAdicionaisPacienteDao;
import br.com.uem.poo.clinica.repository.PacienteDao;
import br.com.uem.poo.clinica.repository.ProntuarioDao;
import java.time.LocalDate;

import java.util.List;
import java.util.stream.Collectors;

public class Medico {
  private PacienteDao pacienteDao;
  private DadosAdicionaisPacienteDao dadosAdicionaisPacienteDao;
  private ProntuarioDao prontuarioDao;

  public Medico(){
    prontuarioDao = new ProntuarioDaoImpl();
    pacienteDao = new PacienteDaoImpl();
    dadosAdicionaisPacienteDao = new DadosAdicionaisDaoImpl();
  }

  public Prontuario buscaProntuarioPeloId(Long id){
    return prontuarioDao.findById(id);
  }

  public void atualizaProntuario(Prontuario prontuario){
    prontuarioDao.replace(prontuario);
  }

  public void removeProntuario(Long id){
    prontuarioDao.delete(id);
  }

  public void adicionaProntuario(Prontuario prontuario){
    prontuarioDao.add(prontuario);
  }

  public List<Prontuario> listaProntuarios(){
    return prontuarioDao.findAll();
  }
  
  public List<Prontuario> listaProntuariosDeUmPaciente(Paciente paciente){
    return listaProntuarios().stream()
            .filter(p->p.getPaciente().equals(paciente))
            .collect(Collectors.toList());
  }
  
  public List<Paciente> listaPacientesDeUmMedicoAtendidosNoMesAtual(String nomeDoMedico){
    LocalDate ld = LocalDate.now();
    return listaProntuarios().stream()
            .filter(p -> p.getNomeMedico().toLowerCase().equals(nomeDoMedico.toLowerCase()))
            .filter((Prontuario p)->p.getData().getYear()==ld.getYear())
            .filter(p->p.getData().getMonth().equals(ld.getMonth()))
            .map(Prontuario::getPaciente)
            .distinct()
            .collect(Collectors.toList());
  }
  
  public List<Paciente> listaPacientes(){
    return pacienteDao.findAll();
  }
  
  public Paciente buscaPacientePeloId(Long id){
    return pacienteDao.findById(id);
  }
   
   public DadosAdicionaisPaciente buscaDadoPeloId(Long id){
    return dadosAdicionaisPacienteDao.findById(id);
  }
   
  public void adicionaDadosAdicional(DadosAdicionaisPaciente dap){
    dadosAdicionaisPacienteDao.add(dap);
  }

  public void removeDadosAdicional(Long id){
    dadosAdicionaisPacienteDao.delete(id);
  }
  
  public void atualizaDadosAdicional(DadosAdicionaisPaciente dap){
    dadosAdicionaisPacienteDao.replace(dap);
  }
}
