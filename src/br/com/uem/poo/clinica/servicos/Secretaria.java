package br.com.uem.poo.clinica.servicos;

import br.com.uem.poo.clinica.dao.ConsultaDaoImpl;
import br.com.uem.poo.clinica.dao.PacienteDaoImpl;
import br.com.uem.poo.clinica.entidade.Consulta;
import br.com.uem.poo.clinica.entidade.Paciente;
import br.com.uem.poo.clinica.repository.ConsultaDao;
import br.com.uem.poo.clinica.repository.PacienteDao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Secretaria {
  private ConsultaDao consultaDao;
  private PacienteDao pacienteDao;

  public Secretaria(){
    consultaDao = new ConsultaDaoImpl();
    pacienteDao = new PacienteDaoImpl();
  }

  public void adicionaConsulta(Consulta consulta){
    consultaDao.add(consulta);
  }

  public List<Consulta> listaConsulta(){
    return consultaDao.findAll();
  }

  public List<Consulta> listaConsultaPelaData(LocalDateTime ldt){
    return listaConsulta().stream()
            .filter(c -> c.getDiaHorario().equals(ldt))
            .collect(Collectors.toList());
  }

  public List<Consulta> listaConsultaPelaData(LocalDate ldt){
    return listaConsulta().stream()
            .filter(c -> c.getDiaHorario().toLocalDate().equals(ldt))
            .collect(Collectors.toList());
  }

  public List<Consulta> listaConsultaPeloEmail(String email){
    return listaConsulta().stream()
            .filter(c -> c.getPaciente().getContato().getEmail().contains(email))
            .sorted(Comparator.comparing(Consulta::getDiaHorario).reversed())
            .collect(Collectors.toList());
  }

  public List<Consulta> listaConsultaPeloMesAno(Integer numeroMes, Integer ano){
    return listaConsulta().stream()
            .filter(c->c.getDiaHorario().getMonthValue()==numeroMes)
            .filter(c->c.getDiaHorario().getYear()==ano)
            .sorted(Comparator.comparing(Consulta::getDiaHorario).reversed())
            .collect(Collectors.toList());
  }

  public List<Consulta> listaConsultaPeloMedicoEPeloMesAno(String nomeMedico, Integer numeroMes, Integer ano){
    return listaConsulta().stream()
            .filter(c -> c.getNomeMedico().equals(nomeMedico))
            .filter(c->c.getDiaHorario().getMonthValue()==numeroMes)
            .filter(c->c.getDiaHorario().getYear()==ano)
            .sorted(Comparator.comparing(Consulta::getDiaHorario).reversed())
            .collect(Collectors.toList());
  }

  public List<Paciente> listaPacientesPeloMedicoEPeloMesAno(String nomeMedico, Integer numeroMes, Integer ano){
    return listaConsulta().stream()
            .filter(c -> c.getNomeMedico().equals(nomeMedico))
            .filter(c->c.getDiaHorario().getMonthValue()==numeroMes)
            .filter(c->c.getDiaHorario().getYear()==ano)
            .sorted(Comparator.comparing(Consulta::getDiaHorario).reversed())
            .map(Consulta::getPaciente)
            .distinct()
            .collect(Collectors.toList());
  }


  public List<Consulta> listaConsultaPeloTelefone(String telefone){
    return listaConsulta().stream()
            .filter(c -> c.getPaciente().getContato().getTelefone().contains(telefone))
            .sorted(Comparator.comparing(Consulta::getDiaHorario).reversed())
            .collect(Collectors.toList());
  }
  
  public List<Consulta> listaConsultaPeloNomePaciente( String str){
   return listaConsulta().stream()
           .filter(c -> c.getPaciente().getNome().contains(str))
           .collect(Collectors.toList());
  }

  public Consulta buscaConsultaPeloId(Long id){
   return consultaDao.findById(id);
  }

  public void atualizaConsulta(Consulta consulta){
    consultaDao.replace(consulta);
  }

  public void removeConsulta(Long id){
    consultaDao.delete(id);
  }

  public void adicionaPaciente(Paciente paciente){
    pacienteDao.add(paciente);
  }

  public List<Paciente> listaPacientes(){
    return pacienteDao.findAll();
  }
  
   public List<Paciente> buscaPacientePeloNome(String name) {
       return listaPacientes().stream()
                .filter(p -> p.getNome().contains(name))
                .collect(Collectors.toList());
    }

  public Paciente buscaPacientePeloId(Long id){
    return pacienteDao.findById(id);
  }

  public void atualizaPaciente(Paciente paciente){
    pacienteDao.replace(paciente);
  }

  public void removePaciente(Long id){
    pacienteDao.delete(id);
  }
}
