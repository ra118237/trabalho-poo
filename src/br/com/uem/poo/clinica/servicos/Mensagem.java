package br.com.uem.poo.clinica.servicos;

import br.com.uem.poo.clinica.dao.ConsultaDaoImpl;
import br.com.uem.poo.clinica.entidade.Consulta;
import br.com.uem.poo.clinica.entidade.Paciente;
import br.com.uem.poo.clinica.mensagem.Mensageiro;
import br.com.uem.poo.clinica.mensagem.email.EmailMensagem;
import br.com.uem.poo.clinica.mensagem.sms.SmsMensagem;
import br.com.uem.poo.clinica.repository.ConsultaDao;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class Mensagem {
    private ConsultaDao consultaDao;
    
    public Mensagem(){
        consultaDao = new ConsultaDaoImpl();
    }
    
    public void enviaEmailParaPacientesComUmDiaDeAntecedencia(String emailRemetente, String assunto, String msg){
        LocalDate ld =  LocalDate.now();
        final LocalDate proximoDia = ld.plusDays(1);
        
        
        List<Mensageiro> emails = consultaDao.findAll().stream()
                .filter(p->p.getDiaHorario().toLocalDate().equals(proximoDia))
                .map(Consulta::getPaciente)
                .map(Paciente::getContato)
                .map(c -> new EmailMensagem(c.getEmail(), emailRemetente,assunto, msg))
                .collect(Collectors.toList());
        
        emails.forEach(e->{
            e.enviaMensagem();
        });
        
    }
    
   public void enviaSmsParaPacientesComUmDiaDeAntecedencia(String telefoneRemetente, String assunto, String msg){
        LocalDate ld =  LocalDate.now();
        final LocalDate proximoDia = ld.plusDays(1);
        
        
        List<Mensageiro> emails = consultaDao.findAll().stream()
                .filter(p->p.getDiaHorario().toLocalDate().equals(proximoDia))
                .map(Consulta::getPaciente)
                .map(Paciente::getContato)
                .map(c -> new SmsMensagem(c.getTelefone(), telefoneRemetente, msg))
                .collect(Collectors.toList());
        
        emails.forEach(e->{
            e.enviaMensagem();
        });
        
    }
}
