package br.com.uem.poo.clinica.repository;

import br.com.uem.poo.clinica.entidade.Paciente;
import java.util.List;

public interface PacienteDao extends Dao<Paciente, Long>{
   
}
