
package br.com.uem.poo.clinica.repository;

import java.io.Serializable;
import java.util.List;

public interface Dao<T extends Serializable, L> {
    void add(T object);
    void replace(T object);
    void delete(L id);
    List<T> findAll();
    T findById(L id);
}
