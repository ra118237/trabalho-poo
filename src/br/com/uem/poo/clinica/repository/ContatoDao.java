package br.com.uem.poo.clinica.repository;

import br.com.uem.poo.clinica.entidade.Contato;

public interface ContatoDao extends Dao<Contato, Long>{
}
