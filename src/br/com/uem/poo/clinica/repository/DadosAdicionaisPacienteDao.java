package br.com.uem.poo.clinica.repository;

import br.com.uem.poo.clinica.entidade.DadosAdicionaisPaciente;

public interface DadosAdicionaisPacienteDao extends Dao<DadosAdicionaisPaciente, Long> {
}
