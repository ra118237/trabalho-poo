package br.com.uem.poo.clinica.repository;

import br.com.uem.poo.clinica.entidade.Prontuario;

public interface ProntuarioDao extends Dao<Prontuario, Long> {
}
