package br.com.uem.poo.clinica.repository;

import br.com.uem.poo.clinica.entidade.Consulta;

public interface ConsultaDao extends Dao<Consulta, Long>{
}
