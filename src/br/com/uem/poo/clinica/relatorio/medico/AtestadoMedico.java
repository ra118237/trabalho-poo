package br.com.uem.poo.clinica.relatorio.medico;

import br.com.uem.poo.clinica.entidade.Paciente;
import br.com.uem.poo.clinica.relatorio.Relatorio;
import br.com.uem.poo.clinica.util.DateTimeUtil;

import java.time.LocalDate;

public class AtestadoMedico implements Relatorio {
  private String paciente;
  private LocalDate data;

  public AtestadoMedico(String paciente, LocalDate data) {
    this.paciente = paciente;
    this.data = data;
  }

  public String getString() {
    return paciente;
  }

  public void setString(String paciente) {
    this.paciente = paciente;
  }

  public LocalDate getData() {
    return data;
  }

  public void setData(LocalDate data) {
    this.data = data;
  }

  @Override
  public String geraRelatorio() {
    StringBuilder sb = new StringBuilder();
    String dataString = DateTimeUtil.converteLocalDateParaString(data, "dd/MM/yyyy");
    sb.append("****  Saúde & Cia  ****\n");
    sb.append("     ATESTADO MEDICO\n");
    sb.append("Nome Paciente: "+paciente+"\n");
    sb.append("Data: "+ dataString +"\n");

    return sb.toString();
  }
}
