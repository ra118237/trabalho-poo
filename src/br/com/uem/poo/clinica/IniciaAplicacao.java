package br.com.uem.poo.clinica;

import br.com.uem.poo.clinica.gui.TelaPrincipal;
import br.com.uem.poo.clinica.util.JPAUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;

import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

public class IniciaAplicacao {
 public static void main(String[] args) {
        //lookTest();
        EntityManagerFactory emf =JPAUtil.getEntityManagerFactory();
        TelaPrincipal tela=new TelaPrincipal();
        tela.setSize( 1000,700);
        tela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tela.setVisible(true);
    }
    private static void lookTest(){
        try {
//            BasicLookAndFeel darcula=new com.bulenkov.darcula.DarculaLaf();
            javax.swing.UIManager.setLookAndFeel(new NimbusLookAndFeel());
        }
        catch(ExceptionInInitializerError ex){
            System.out.println("Erro inicializar => "+ex.getMessage());
            
        } catch (UnsupportedLookAndFeelException ex) {
         Logger.getLogger(IniciaAplicacao.class.getName()).log(Level.SEVERE, null, ex);
     }
    }
}
